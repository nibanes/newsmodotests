using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class CheckPitchInPitchTable : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        public Pitch pitch;
        
        // Add your test methods here...
    
        [CodedStep(@"Get Pitch Details")]
        public void GetPitchDetails_CodedStep()
        {
            // read pitch details from file
            var content = FileReaderUtility.ReadFile(Data["PitchLogfile"].ToString());
            this.pitch = JsonConvert.DeserializeObject<Pitch>(content);
        }    
        
        [CodedStep(@"Check Pitch Table")]
        public void CheckPitchTable_CodedStep()
        {
            int i = 0;
            while(true)
            {
                ActiveBrowser.RefreshDomTree();
                Log.WriteLine(string.Format("Searching for {0} in Page {1}", this.pitch.Title, i.ToString()));
                HtmlTableCell cell = Pages.NewsmodoAdmin.PitchesListDiv.Find.ByExpression<HtmlTableCell>(string.Format("textcontent={0}", this.pitch.Title));
                if(cell != null)
                {
                    Log.WriteLine(string.Format("***TRACE: Cell found:: Title {0}", cell.InnerText));                    
                    var row = cell.Parent<HtmlTableRow>();
                    var cells = new List<HtmlTableCell> (row.Find.AllControls<HtmlTableCell>());                    
                    Assert.IsTrue(pitch.Status.Equals(cells[3].TextContent), string.Format("***ERROR: Pitch status mismatch! [Exp]: {0} != [Act]: {1}", pitch.Status, cells[3].TextContent));
                    Assert.IsTrue(pitch.Reporter.Email.Equals(cells[4].TextContent), string.Format("***ERROR: Pitch email mismatch! [Exp]: {0} != [Act]: {1}", pitch.Reporter.Email, cells[4].TextContent));
                    Assert.IsTrue(pitch.Title.Equals(cells[5].TextContent), string.Format("***ERROR: Pitch title mismatch! [Exp]: {0} != [Act]: {1}", pitch.Title, cells[5].TextContent));
                    break;
                }
                else
                {
                    Log.WriteLine(string.Format("Cell NOT found in Page {0}", i.ToString()));
                    Pages.NewsmodoAdmin.PagerButtonSpan.ScrollToVisible();
                    Pages.NewsmodoAdmin.PagerButtonSpan.MouseClick();                    
                }
                i++;                
            }
        }
    }
}
