using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class CheckPitchDetails : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        public Pitch expected;
        
        // Add your test methods here...
    
        [CodedStep(@"Get Pitch Details")]
        public void GetPitchDetails_CodedStep()
        {
            // read pitch details from file
            var content = FileReaderUtility.ReadFile(Data["PitchLogfile"].ToString());
            this.expected = JsonConvert.DeserializeObject<Pitch>(content);
        }
        
        [CodedStep(@"Check Pitch Details")]
        public void CheckPitchDetails_CodedStep()
        {
            ActiveBrowser.RefreshDomTree();            
            Pages.NewsmodoAdmin.PitchPitcherNameDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Name:" + expected.Reporter.FirstName + " " + expected.Reporter.LastName);
            Pages.NewsmodoAdmin.PitchPitcherLocationDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Location:Australia");
            Pages.NewsmodoAdmin.PitchPitcherPhoneDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Phone:" + expected.Reporter.Phone);
            Pages.NewsmodoAdmin.PitchPicherEmailDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Email:" + expected.Reporter.Email);
            Pages.NewsmodoAdmin.PitchTitleDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Title:" + expected.Title);
            Pages.NewsmodoAdmin.PitchStatusDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Status:" + expected.Status);
            Pages.NewsmodoAdmin.PitchLocationDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Location:" + expected.Location);
            Pages.NewsmodoAdmin.PitchFeeDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Fee:" + expected.Price);
            Pages.NewsmodoAdmin.PitchPoint1Div.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Point 1:" + expected.Point1);
            Pages.NewsmodoAdmin.PitchPoint2Div.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Point 2:"  + expected.Point2);
            Pages.NewsmodoAdmin.PitchPoint3Div.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Point 3:" +  expected.Point3);
            Pages.NewsmodoAdmin.PitchSampleQuotesDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Sample Quotes:" + expected.Quotes);
            Pages.NewsmodoAdmin.PitchPitchedStoryDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Pitched Story:" + expected.Description);            
            Pages.NewsmodoAdmin.PitchPitchUrlDiv.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, "Url:Pitch URL");
            
            Log.WriteLine("***TRACE: Checking of pitch details passed");
        }
    }
}
