using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class TestE2EUnsolicitedPitch : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        Pitch pitch;
        
        [CodedStep(@"Get Pitch Details")]
        public void GetPitchDetails_CodedStep()
        {
            // read pitch details from file
            var content = FileReaderUtility.ReadFile(Data["PitchLogfile"].ToString());
            this.pitch = JsonConvert.DeserializeObject<Pitch>(content);            
        }    
        
        [CodedStep(@"Select Buyers")]
        public void SelectBuyers_CodedStep()
        {
            Pages.NewsmodoAdmin.SelectedH3Tag.AssertContent().InnerText(ArtOfTest.Common.StringCompareType.Exact, string.Format("Selected pitch to send: {0}", this.pitch.Title));            
            
            // select random checkbox
            var cbAry = new List<ArtOfTest.WebAii.Controls.HtmlControls.HtmlInputCheckBox> (Pages.NewsmodoAdmin.PitchSendToBuyerTable.Find.AllControls<ArtOfTest.WebAii.Controls.HtmlControls.HtmlInputCheckBox>());
            
            bool norecipient = true;
            int i = 0;
            foreach(ArtOfTest.WebAii.Controls.HtmlControls.HtmlInputCheckBox cb in cbAry)
            {
                cb.ScrollToVisible();
                if(Randomizer.GetRandomBool())
                {
                    cb.MouseClick();
                    norecipient = false;
                    i += 1;
                }
            }
            
            // if no recipient or if all checkboxes were clicked
            if(norecipient || i == cbAry.Count())
            {
                cbAry[0].ScrollToVisible();
                cbAry[0].MouseClick();
            }
        }
    
        [CodedStep(@"Verify pitch status in database")]
        public void VerifyPitchStatus_CodedStep()
        {
            pitch.Deleted = false;
            pitch.AlreadySent = true;
            pitch.Status = "SendToCrm";
            
            string success = RavenClient.VerifyPitchStatus(this.pitch);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR: Status mismatch for sent pitch! \r\n {0}", success));            
        }
    }
}
