using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class TestSetAPublicBrief : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        public Brief Brief;
    
        [CodedStep(@"Fetch Brief Details")]
        public void FetchBriefDetails_CodedStep()
        {
            var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
            this.Brief = JsonConvert.DeserializeObject<Brief>(content);
        }        
    
        [CodedStep(@"Verify Brief Status")]
        public void VerifyBriefStatus_CodedStep()
        {            
            System.Threading.Thread.Sleep(8000);
            
            // verify in database
            this.Brief.Status = "Public";
            this.Brief.AdminStatus = "New";           
            
            string success = RavenClient.VerifyBriefStatus(this.Brief);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR: Status mismatch for sent brief! \r\n {0}", success));
            
            // check in UI
            ActiveBrowser.RefreshDomTree();
            
            bool found = false;
            while(!found)
            {
                int i = 0;
                ActiveBrowser.RefreshDomTree();
                Log.WriteLine(string.Format("Searching for {0} in Page {1}", this.Brief.Title, i.ToString()));
                HtmlTableCell cell = Pages.NewsmodoAdmin.BriefsListDiv.Find.ByExpression<HtmlTableCell>(string.Format("textcontent={0}", this.Brief.Title));                
                if(cell != null)
                {
                    Log.WriteLine(string.Format("***TRACE: Cell found:: Title {0}", cell.InnerText));
                    cell.ScrollToVisible();
                    var row = cell.Parent<HtmlTableRow>();
                    var cells = new List<HtmlTableCell> (row.Find.AllControls<HtmlTableCell>());
                    Assert.IsTrue(cells[6].TextContent.Equals("Public"), string.Format("***ERRROR: Brief status not set to Public! [Act] : {0}", cells[6].TextContent));
                    found = true;
                }                
                else
                {
                    Log.WriteLine(string.Format("***TRACE: Cell NOT found in Page {0}", i.ToString()));
                    Pages.NewsmodoAdmin.PagerButtonSpan.ScrollToVisible();
                    Pages.NewsmodoAdmin.PagerButtonSpan.MouseClick();
                    System.Threading.Thread.Sleep(2000);
                }
                i++;
            }
        }
    }
}
