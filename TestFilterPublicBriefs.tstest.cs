using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class TestFilterPublicBriefs : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        [CodedStep(@"Filter Public Briefs")]
        public void FilterPublicBriefs_CodedStep()
        {
            for(int i = 0; i < 5; i++)
            {                
                string country = "";
                switch(i)
                {                    
                    case 0: country = "All"; break;
                    case 1: country = "Global"; break;
                    default: country = Randomizer.GetRandomCountry(); break;
                }
                
                Log.WriteLine(string.Format("***TRACE: Filtering by country: {0}", country));
                
                if(i > 0)
                {
                    // select country
                    Pages.NEWSMODOCURRENTBRIEFS.CountrySelect.SelectByValue(country, true);
                    //ArtOfTest.WebAii.jQuery.jQueryControl jQueryControl = Pages.NEWSMODOCURRENTBRIEFS.CountrySelect.AsjQueryControl();
                    //jQueryControl.InvokejQueryEvent(ArtOfTest.WebAii.jQuery.jQueryControl.jQueryControlEvents.change);
                    System.Threading.Thread.Sleep(5000);
                }
                
                // refresh elements
                ActiveBrowser.RefreshDomTree();
                
                // get briefs list based on country selected
                IList<Brief> results = RavenClient.GetBriefsBasedOnCountry(country);
                Log.WriteLine(string.Format("***TRACE: Expected count: {0}", results.Count));
                
                // check completeness and accuracy
                if(results.Count == 0)
                {
                    var e = Pages.NEWSMODOCURRENTBRIEFS.Find.ByContent("p:No briefs found");
                    
                    if(e != null)
                        Log.WriteLine("***TRACE: No briefs found.");
                    else
                        Assert.IsTrue(true, "***ERROR: Expecting No briefs found!");
                    
                    // div containing briefs list should not show-up
                    //Assert.IsFalse(Pages.NEWSMODOCURRENTBRIEFS.PublicBriefsListSectionTag.IsVisible(), "Div containing briefs must not be visible!");
                }
                
                else
                {
                    foreach(Brief b in results)
                    {
                        var e = Pages.NEWSMODOCURRENTBRIEFS.PublicBriefsListSectionTag.Find.ByContent(b.Title);
                        
                        if(e != null)
                            Log.WriteLine(string.Format("***TRACE: Brief {0} found.", b.Title));
                        else
                            Assert.IsTrue(true, string.Format("***ERROR: Brief {0} NOT found!", b.Title));
                    }
                    
                    IList<HtmlDiv> divs = Pages.NEWSMODOCURRENTBRIEFS.PublicBriefsListSectionTag.Find.AllControls<HtmlDiv>().ToList<HtmlDiv>();
                    
                    Log.WriteLine(string.Format("***TRACE: Actual count: {0}", divs.Count));
                    Assert.IsTrue(results.Count == divs.Count, string.Format("[Exp] Count: {0} != [Act] Count: {1}", results.Count, divs.Count));
                }
            }
        }
    }
}
