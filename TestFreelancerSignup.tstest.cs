using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class TestFreelancerSignup : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
        Pitcher reporter;
    
        [CodedStep(@"Generate Pitcher")]
        public void GeneratePitcher_CodedStep()
        {
            if("New".Equals(Data["BriefType"].ToString()))
                this.reporter = RavenClient.GetNextFreelancer(Data["TestName"].ToString());
            else if("Random".Equals(Data["BriefType"].ToString()))
                this.reporter = RavenClient.GetRandomPitcher(Data["TestName"].ToString());
            else
            {
                var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
                this.reporter = JsonConvert.DeserializeObject<Pitcher>(content);
                this.reporter.Update("Updated");                
            }
            Log.WriteLine(string.Format("Signing-up freelancer {0}", JsonConvert.SerializeObject(reporter)));
        }
    
        [CodedStep(@"Enter Pitcher Details")]
        public void EnterPitcherDetails_CodedStep()
        {
            Actions.SetText(Pages.NEWSMODO.PitcherSignupFirstNameText, reporter.FirstName);
            Actions.SetText(Pages.NEWSMODO.PitcherSignupLastNameText, reporter.LastName);            
            Actions.SetText(Pages.NEWSMODO.PitcherSignupEmailText, reporter.Email);
        }
    
        [CodedStep(@"Verify Pitcher")]
        public void VerifyPitcher_CodedStep()
        {
            var success = RavenClient.SearchPitcher(this.reporter);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("VerifyPitcher():: Search pitcher error! {0}", success));            
        }
    }
}
