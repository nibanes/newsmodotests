using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class SetSolicitedPitchToPrivateBrief : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        Pitch pitch;
        Brief brief;

        [CodedStep(@"Generate pitch details")]
        public void GeneratePitch_CodedStep()
        {            
            // generate pitch details            
            this.pitch = RavenClient.GetNextPitch(Data["PitchLogfile"].ToString(), true, false);
        }
    
        [CodedStep(@"Navigate to Pitch Form")]
        public void NavigateToPitchForm_CodedStep()
        {
            var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
            this.brief = JsonConvert.DeserializeObject<Brief>(content);
            
            string id = RavenClient.GetBriefId(this.brief.Title).Split('/')[1];            
            Log.WriteLine(string.Format("Brief Id {0}:: {1}", id, this.brief.Title));
            
            //string url = string.Format("http://api12.newsmodo.com/Story?briefId={0}", System.Net.WebUtility.UrlEncode(RijndaelEncryption.Encrypt(id)));
            
            string url = string.Format("http://api12.newsmodo.com/Story?pitcherId={0}&briefId={1}", 
                System.Net.WebUtility.UrlEncode(RijndaelEncryption.Encrypt(this.pitch.Reporter.Id.Split('/')[1])),
                System.Net.WebUtility.UrlEncode(RijndaelEncryption.Encrypt(id)));
                
            
            // navigate to private brief page
            Log.WriteLine("Navigating to URL: " + url);
            ActiveBrowser.NavigateTo(url, false);

            // verify pre-populated details
            Pages.NEWSMODOPITCHASTORY.PitcherFirstNameText.Wait.ForVisible(30000);
            Pages.NEWSMODOPITCHASTORY.PitcherFirstNameText.ScrollToVisible();
            Assert.IsTrue(Pages.NEWSMODOPITCHASTORY.PitcherFirstNameText.Text.Equals(pitch.Reporter.FirstName), string.Format("***ERROR: Pre-filled FirstName [Exp] {0} : [Act] {1}", pitch.Reporter.FirstName, Pages.NEWSMODOPITCHASTORY.PitcherFirstNameText.Text));
            Assert.IsTrue(Pages.NEWSMODOPITCHASTORY.PitcherLastNameText.Text.Equals(pitch.Reporter.LastName), string.Format("***ERROR: Pre-filled LastName [Exp] {0} : [Act] {1}", pitch.Reporter.LastName, Pages.NEWSMODOPITCHASTORY.PitcherLastNameText.Text));
            Assert.IsTrue(Pages.NEWSMODOPITCHASTORY.PitcherEmailText.Text.Equals(pitch.Reporter.Email), string.Format("***ERROR: Pre-filled Email [Exp] {0} : [Act] {1}", pitch.Reporter.Email, Pages.NEWSMODOPITCHASTORY.PitcherEmailText.Text));            
            
            pitch.Reporter.Phone = (string.IsNullOrEmpty(pitch.Reporter.Phone) || string.IsNullOrWhiteSpace(pitch.Reporter.Phone))? string.Empty: pitch.Reporter.Phone;
            var phone = (string.IsNullOrEmpty(Pages.NEWSMODOPITCHASTORY.PitcherPhoneText.Text) || string.IsNullOrWhiteSpace(Pages.NEWSMODOPITCHASTORY.PitcherPhoneText.Text))? string.Empty: Pages.NEWSMODOPITCHASTORY.PitcherPhoneText.Text;
            Assert.IsTrue(phone.Equals(pitch.Reporter.Phone), string.Format("***ERROR: Pre-filled Phone [Exp] {0} : [Act] {1}", pitch.Reporter.Phone, phone));
            Assert.IsTrue(Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.Text.Equals(id), string.Format("***ERROR: Brief id number is incorrect! [Exp] {0} : [Act] {1}", id, Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.Text));
        }
        
        [CodedStep(@"Validate created pitch")]
        public void ValidatePitch_CodedStep()
        {
            string success = RavenClient.SearchPitch(this.pitch, this.brief.Title);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR with created pitch! \r\n {0}", success));            
        }
    }
}
