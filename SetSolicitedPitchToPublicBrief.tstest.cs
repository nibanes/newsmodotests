using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class SetSolicitedPitchToPublicBrief : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        Pitch pitch;
        Brief brief;

        [CodedStep(@"Get Brief Title")]
        public void GetBriefTitle_CodedStep()
        {            
            var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
            this.brief = JsonConvert.DeserializeObject<Brief>(content);
            this.brief.Id = RavenClient.GetBriefId(this.brief.Title);
        }

        [CodedStep(@"Generate pitch details")]
        public void GeneratePitch_CodedStep()
        {            
            // generate pitch details            
            this.pitch = RavenClient.GetNextPitch(Data["PitchLogfile"].ToString(), true);
        }
    
        [CodedStep(@"Select Brief")]
        public void SelectBrief_CodedStep()
        {            
            var divs = Pages.NEWSMODOCURRENTBRIEFS.PublicBriefsListSectionTag.Find.AllControls<HtmlDiv>();
            
            foreach(HtmlDiv div in divs)
            {
                bool found = false;
                var list = div.Find.AllControls<HtmlControl>();
                HtmlControl button = null;
                foreach(HtmlControl c in list)
                {
                    // get button handle
                    if(c.BaseElement.InnerText.Contains("PITCH FOR THIS BRIEF"))
                    {
                        button = c;
                        Log.WriteLine("Button found!");
                    }
                    if(c.BaseElement.InnerText.Contains(this.brief.Title))
                    {
                        found = true;
                        Log.WriteLine(string.Format("{0} Found!", this.brief.Title));
                    }
                }
                if(found)
                {
                    button.ScrollToVisible();
                    button.MouseClick();
                    break;
                }
            }
            
            Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.Wait.ForVisible(30000);
            Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.ScrollToVisible();
            Assert.IsTrue(Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.Text.Equals(this.brief.Id.Split('/')[1]), string.Format("***ERROR: Brief id number is incorrect! [Exp] {0} : [Act] {1}", this.brief.Id.Split('/')[1], Pages.NEWSMODOPITCHASTORY.PitchBriefIdNumberText.Text));
        }
        
        [CodedStep(@"Validate created pitch")]
        public void ValidatePitch_CodedStep()
        {
            string success = RavenClient.SearchPitch(this.pitch, this.brief.Title);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR with created pitch! \r\n {0}", success));            
        }
    }
}
