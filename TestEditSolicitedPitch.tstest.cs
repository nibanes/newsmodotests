using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class TestEditSolicitedPitch : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        public Pitch pitch;
        
        // Add your test methods here...
        
        [CodedStep(@"Get Pitch Details")]
        public void GetPitchDetails_CodedStep()
        {
            // read pitch details from file
            var content = FileReaderUtility.ReadFile(Data["PitchLogfile"].ToString());
            this.pitch = JsonConvert.DeserializeObject<Pitch>(content);            
        }
    
        
    
        [CodedStep(@"Cancelled Changes Scenario")]
        public void CancelledChangesScenario_CodedStep()
        {
            this.ExecuteTest("ActionsMouseClick\\ClickEditPitchToBriefButton.tstest");
            
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefTitleText, string.Format("{0} Cancelled", pitch.Title));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefLocationText, string.Format("{0} Cancelled", pitch.Location));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPriceText, string.Format("{0} Cancelled", pitch.Price));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint1TextArea, string.Format("{0} Cancelled", pitch.Point1));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint2TextArea, string.Format("{0} Cancelled", pitch.Point2));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint3TextArea, string.Format("{0} Cancelled", pitch.Point3));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefQuotesTextArea, string.Format("{0} Cancelled", pitch.Quotes));
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefDescriptionTextArea, string.Format("{0} Cancelled", pitch.Description));
            
            this.ExecuteTest("ActionsMouseClick\\ClickCancelEditPitch.tstest");

            // verify loaded pitch
            this.ExecuteTest("ActionsVerification\\CheckPitchToBriefDetails.tstest");
            Log.WriteLine("***TRACE: Checking of cancelled pitch to brief changes in UI passed");
            
            // check that changes weren't saved in database
            string success = RavenClient.SearchPitch(this.pitch);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR: Cancelled Changes - Error observed with searched pitch! \r\n {0}", success));            
            Log.WriteLine("***TRACE: Checking of cancelled pitch changes in database passed");
        }
    
        [CodedStep(@"Saved Changes Scenario")]
        public void SavedChangesScenario_CodedStep()
        {
            this.ExecuteTest("ActionsMouseClick\\ClickEditPitchToBriefButton.tstest");
            
            this.pitch.Title = string.Format("{0} Edited", this.pitch.Title);
            this.pitch.Location = Randomizer.GetRandomLocation();
            this.pitch.Price = string.Format("{0} Edited", this.pitch.Price);
            this.pitch.Point1 = string.Format("{0} Edited", this.pitch.Point1);
            this.pitch.Point2 = string.Format("{0} Edited", this.pitch.Point2);
            this.pitch.Point3 = string.Format("{0} Edited", this.pitch.Point3);
            this.pitch.Quotes = string.Format("{0} Edited", this.pitch.Quotes);
            this.pitch.Description = string.Format("{0} Edited", this.pitch.Description);

            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefTitleText, pitch.Title);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefLocationText, pitch.Location);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPriceText, pitch.Price);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint1TextArea, pitch.Point1);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint2TextArea, pitch.Point2);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefPoint3TextArea, pitch.Point3);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefQuotesTextArea, pitch.Quotes);
            Actions.SetText(Pages.NewsmodoAdmin.PitchToBriefDescriptionTextArea, pitch.Description);
            
            this.ExecuteTest("ActionsMouseClick\\ClickSavePitchToBriefButton.tstest");
            System.IO.File.WriteAllText(string.Format("C:\\Users\\Public\\{0}.txt", Data["PitchLogfile"].ToString()), JsonConvert.SerializeObject(this.pitch));

            // verify loaded pitch
            this.ExecuteTest("ActionsVerification\\CheckPitchToBriefDetails.tstest");
            Log.WriteLine("***TRACE: Checking of saved pitch to brief changes in UI passed");

            string success = RavenClient.SearchPitch(this.pitch);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR: Saved Changes - Error observed with searched pitch! \r\n {0}", success));
            Log.WriteLine("***TRACE: Checking of saved pitch changes in database passed");
        }
    }
}
