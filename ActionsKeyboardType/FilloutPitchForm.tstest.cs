using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class FilloutPitchForm : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
        [CodedStep(@"Fill-out Pitch Form")]
        public void FilloutPitchForm_CodedStep()
        {
            // read pitch details from file
            var content = FileReaderUtility.ReadFile(Data["PitchLogfile"].ToString());
            var pitch = JsonConvert.DeserializeObject<Pitch>(content);
            
            // fill-out pitch form
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitcherFirstNameText, pitch.Reporter.FirstName);            
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitcherLastNameText, pitch.Reporter.LastName);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitcherEmailText, pitch.Reporter.Email);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitcherPhoneText, pitch.Reporter.Phone);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchTitleText, pitch.Title);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchLocationText, pitch.Location);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchPoint1TextArea, pitch.Point1);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchPoint2TextArea, pitch.Point2);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchPoint3TextArea, pitch.Point3);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchQuotesTextArea, pitch.Quotes);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchPriceText, pitch.Price);
            Actions.SetText(Pages.NEWSMODOPITCHASTORY.PitchDescriptionTextArea, pitch.Description);
        }    
    }
}
