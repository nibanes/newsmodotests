using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class FilloutBriefForm : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
    
        [CodedStep(@"Fill-out Brief Form")]
        public void FilloutBriefForm_CodedStep()
        {
            var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
            var Brief = JsonConvert.DeserializeObject<Brief>(content);
            
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BuyerFirstNameText, Brief.Publisher.FirstName);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BuyerLastNameText, Brief.Publisher.LastName);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BuyerEmailText, Brief.Publisher.Email);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BuyerPhoneText, Brief.Publisher.Phone);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BuyerCompanyText, Brief.Publisher.Company);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BriefTitleText, Brief.Title);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BriefLocationText, Brief.Location);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BriefPriceText, Brief.Price);
            Actions.SetText(Pages.NEWSMODOSETABRIEF.BriefDescriptionTextArea, Brief.Description);            
        }
    }
}
