using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

//using System;
using System.IO;
using System.Net;
using System.Security.Cryptography;
//using System.Text;
using System.Web;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class RijndaelEncryption : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        private const PaddingMode _PaddingMode = PaddingMode.PKCS7;
        private const CipherMode _CipherMode = CipherMode.ECB;
        private const string Key = "aglqCz5MdEnPwBIAyA5WNqBZGeTD7CXXYC+qmgB7lIA="; // Base 64 String

        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="value">value</param>
        /// <returns>Html Encode String</returns>
        public static string Encrypt(string value)
        {
            return Encrypt(Key, value);
        }

        /// <summary>
        /// Encrypt
        /// </summary>
        /// <param name="base64StringKey">Base 64 String Key</param>
        /// <param name="value">value</param>
        /// <returns>Html Encode String</returns>
        public static string Encrypt(string base64StringKey, string value)
        {
            var sa = Rijndael.Create();

            sa.Key = Convert.FromBase64String(base64StringKey);
            sa.GenerateIV();
            sa.Mode = _CipherMode;
            sa.Padding = _PaddingMode;

            var ms = new MemoryStream();
            var cs = new CryptoStream(ms, sa.CreateEncryptor(), CryptoStreamMode.Write);

            var plainbytes = Encoding.UTF8.GetBytes(value);

            cs.Write(plainbytes, 0, plainbytes.Length);
            cs.Close();

            var cipherbytes = ms.ToArray();
            ms.Close();

            return Convert.ToBase64String(cipherbytes);
        }        
        
        [CodedStep(@"New Coded Step")]
        public void RijndaelEncryption_CodedStep()
        {
            Log.WriteLine(System.Net.WebUtility.UrlEncode(RijndaelEncryption.Encrypt("2695")));
        }
    }
}
