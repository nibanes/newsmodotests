using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;
using Raven.Client.Document;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //

    public class Location
    {
        public string Country { get; set; }
        public string Region { get; set; }
        public string City { get; set; }
    }
    
    public class Pitcher
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public Location Location { get; set; }
        public bool SendMail { get; set; }
        
        public void Update(string suffix)
        {
            FirstName = FirstName + " " + suffix;
            LastName = LastName + " " + suffix;
            //Phone = Phone + " " + suffix;
        }
    }
    
    public class Pitch
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public string Price { get; set; }
        public string Description { get; set; }
        public string Point1 { get; set; }
        public string Point2 { get; set; }
        public string Point3 { get; set; }
        public DateTime CreatedDate { get; set; }
        public Pitcher Reporter { get; set; }
        public string PitcherId { get; set; }
        public bool IsFavorite { get; set; }
        public string BriefId { get; set; }
        public bool Deleted { get; set; }
        public string Quotes { get; set; }
        public string Url { get; set; }
        public string UniqueId { get; set; }
        public bool AlreadySent { get; set; }
        public string Status { get; set; }
        public bool Repitch { get; set; }
    }
    
    public class Buyer
    {
        public string Id { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Company { get; set; }
        public string Email { get; set; }
        public string Phone { get; set; }
        public bool SendMail { get; set; }
    }
    
    public class Brief
    {
        public string Id { get; set; }
        public string Title { get; set; }
        public Buyer Publisher { get; set; }
        public string AdminStatus { get; set; }
        public string BuyerId { get; set; }
        public bool Commissioned { get; set; }
        public bool Complete { get; set; }
        public DateTime CreatedDate { get; set; }
        public bool Deleted { get; set; }
        public string Description { get; set; }
        public string Location { get; set; }
        public string [] PitchIds { get; set; }
        public string Price { get; set; }
        public bool Sent { get; set; }
        public string Status { get; set; }
        public string Country { get; set; }
    }

    public class RavenClient : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }
        
        #endregion
        
        // Add your test methods here...
        
        public static Raven.Client.IDocumentSession OpenRavenSession()
        {
            var store = new DocumentStore { Url = "https://ibis.ravenhq.com/databases/newsmodo-nmlite",ApiKey="cbb05b6a-da5f-4718-bf6e-1f8163557b37" };
            Assert.IsTrue(store != null, "store is null");
            store.Conventions.MaxNumberOfRequestsPerSession = 1000;
            store.Initialize();
            return store.OpenSession("newsmodo-nmlite");
        }
        
        public static Pitch GetNextPitch(string TestName, bool IsSolicited = true, bool IsNewPitcher = true)
        {
            string path = string.Format("C:\\Users\\Public\\{0}.txt", TestName);
            string name = (IsSolicited)? "SolicitedStoryD" : "UnsolicitedStory";
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitch>() where p.Title.StartsWith(name) select p).ToList<Pitch>();
            int idx = 0;
            foreach(Pitch p in results)
            {                
                if(p.Title != null)
                {
                    if(p.Title.StartsWith(name))
                    {
                        int num = System.Convert.ToInt32(p.Title.Split(' ')[2]);
                        idx = (num > idx)? num: idx;
                    }
                }
            }
            
            idx += 1;
            
            session.Dispose();
            
            var obj = new Pitch() 
            { 
                Title = name + " Pitch " + idx.ToString(),
                Location = Randomizer.GetRandomLocation(),
                Price = idx.ToString(),
                Description = "Pitch Description " + idx.ToString(),
                Point1 = "Pitch Point1 " + idx.ToString(),
                Point2 = "Pitch Point2 " + idx.ToString(),
                Point3 = "Pitch Point3 " + idx.ToString(),
                //Reporter = new Pitcher()
                //{
                //    FirstName = name,
                //    LastName = "Pitcher " + idx.ToString(),
                //    Email = "qa+" + name.ToLower() + "pitcher" + idx.ToString() + "@oxygenventures.com.au",
                //    Phone = name + " Phone " + idx.ToString(),
                //},
                
                Quotes = name + " Quotes " + idx.ToString()
            };
            
            if(IsNewPitcher)
            {
                obj.Reporter = new Pitcher()
                {
                    FirstName = name,
                    LastName = "Pitcher " + idx.ToString(),
                    //Email = "qa+" + name.ToLower() + "pitcher" + idx.ToString() + "@oxygenventures.com.au",
                    Email = "qa+" + name.ToLower() + "pitcher" + "@oxygenventures.com.au",
                    Phone = name + " Phone " + idx.ToString()
                };
            } 
            else
                obj.Reporter = RavenClient.GetRandomPitcher();
            
            obj.AlreadySent = false;
            obj.Deleted = false;
            obj.IsFavorite = false;
            obj.Status = "Received";
            
            System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(obj));            
            return obj;
        }
        
        public static Brief GetNextBrief(string TestName, string BriefType)
        {
            string path = string.Format("C:\\Users\\Public\\{0}.txt", TestName);
            //string name = (IsPrivate)? "Private" : "Public";
            var session = RavenClient.OpenRavenSession();
            //var results = session.Query<Brief>().ToList();
            var results = (from b in session.Query<Brief>() where b.Title.StartsWith(BriefType) select b).ToList<Brief>();
            int idx = 0;
            foreach(Brief b in results)
            {                
                if(b.Title != null)
                {
                    if(b.Title.StartsWith(BriefType))
                    {
                        int num = System.Convert.ToInt32(b.Title.Split(' ')[2]);
                        idx = (num > idx)? num: idx;
                    }
                }
            }
            
            idx += 1;
            
            session.Dispose();
            
            var obj = new Brief() 
            {
                Publisher = new Buyer()
                {
                    FirstName = BriefType,
                    LastName = "Buyer " + idx.ToString(),
                    Company = BriefType + " Company " + idx.ToString(),
                    Email = "qa+" + BriefType.ToLower() + "buyer" + idx.ToString() + "@oxygenventures.com.au",
                    //Email = "qa+" + BriefType.ToLower() + "buyer" + "@oxygenventures.com.au",
                    Phone = BriefType + " Phone " + idx.ToString()
                },
                Description = BriefType + " Brief Description " + idx.ToString(),
                Location = Randomizer.GetRandomLocation(),
                Price = idx.ToString(),
                Title = BriefType + " Brief " + idx.ToString(),
            };
            
            while(true)
            {
                try
                {
                    System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(obj));
                    break;
                }
                catch(System.IO.IOException) {}
                System.Threading.Thread.Sleep(500);
            }
            
            return obj;
        }
        
        public static string VerifyPitchStatus(Pitch expected)
        {
            string success = string.Empty;            
            bool found = false;
            
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitch>() where p.Title.StartsWith(expected.Title) select p).ToList<Pitch>();
            
            foreach(Pitch p in results)
            {                
                if(expected.Title.Equals(p.Title))
                {
                    found = true;
                    if(expected.AlreadySent != p.AlreadySent)
                        success = string.Format("{0}\r\n ***ERROR: AlreadySent mismatch! [Exp] {1} != [Act] {2}", success, expected.AlreadySent.ToString(), p.AlreadySent.ToString());
                    if(!expected.Status.Equals(p.Status))
                        success = string.Format("{0}\r\n ***ERROR: Status mismatch! [Exp] {1} != [Act] {2}", success, expected.Status, p.Status);
                    if(expected.Deleted != p.Deleted)
                        success = string.Format("{0}\r\n ***ERROR: IsDeleted mismatch! [Exp] {1} != [Act] {2}", success, expected.Deleted.ToString(), p.Deleted.ToString());
                    
                    break;
                }
            }
            
            if(!found)
                success = string.Format("{0}\r\n ***ERROR: [VerifyPitchStatus()]Failed to find pitch! [Exp] {1}", success, JsonConvert.SerializeObject(expected));
                
            session.Dispose();
            
            return success;
        }
        
        public static string SearchPitch(Pitch expected)
        {
            return RavenClient.SearchPitch(expected, string.Empty);
        }
        
        public static string SearchPitch(Pitch expected, string BriefTitle)
        {
            string success = string.Empty;
            bool found = false;
            
            var session = RavenClient.OpenRavenSession();
            //var results = session.Query<Pitch>().ToList();
            var results = (from p in session.Query<Pitch>() where p.Title.StartsWith(expected.Title) select p).ToList<Pitch>();
            
            foreach(Pitch p in results)
            {                
                if(expected.Title.Equals(p.Title))
                {
                    found = true;
                    if(!expected.Location.Equals(p.Location) ||
                        !expected.Price.Equals(p.Price) ||
                        !expected.Description.Equals(p.Description) ||
                        !expected.Quotes.Equals(p.Quotes) ||
                        !expected.Point1.Equals(p.Point1) ||
                        !expected.Point2.Equals(p.Point2) ||
                        !expected.Point3.Equals(p.Point3))
                        success = string.Format("{0}\r\n ***ERROR: Incorrect Pitch Details! \r\n [Exp] {1} != \r\n [Act] {2}", success, JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(p));
                    
                    var r = session.Load<Pitcher>(p.PitcherId);
                    
                    if(r != null)
                    {
                        if(!expected.Reporter.FirstName.Equals(r.FirstName) ||
                            !expected.Reporter.LastName.Equals(r.LastName) ||
                            !expected.Reporter.Email.Equals(r.Email) ||
                            !expected.Reporter.Phone.Equals(r.Phone))
                            success = string.Format("{0}\r\n ***ERROR: Incorrect Reporter Details! \r\n [Exp] {1} != \r\n [Act] {2}", success, JsonConvert.SerializeObject(expected.Reporter), JsonConvert.SerializeObject(r));
                    }
                    else
                        success = string.Format("{0}\r\n ***ERROR: Failed to find pitcher with id {1}", success, p.PitcherId);
                    
                    // brief title
                    if(!BriefTitle.Equals(string.Empty))
                    {
                        if(p.BriefId.Equals(""))
                        {
                            success = string.Format("{0}\r\n ***ERROR: Brief ID is empty!", success);
                        }
                        else
                        {
                            var b = session.Load<Brief>(p.BriefId);
                            if(!BriefTitle.Equals(b.Title))
                                success = string.Format("{0}\r\n ***ERROR: Incorrect Brief Title! \r\n [Exp] {1} \r\n [Act] {2}", success, BriefTitle, b.Title);
                        }
                    }
                    
                    // status
                    if(p.Deleted != expected.Deleted)
                        success = string.Format("{0}\r\n ***ERROR: IsDeleted mismatch! [Exp]: {1} != [Act]: {2}", success, expected.Deleted, p.Deleted);
                    if(p.AlreadySent != expected.AlreadySent)
                        success = string.Format("{0}\r\n ***ERROR: AlreadySent mismatch! [Exp]: {1} != [Act]: {2}", success, expected.AlreadySent, p.AlreadySent);
                    if(!p.Status.Equals(expected.Status))
                        success = string.Format("{0}\r\n ***ERROR: Status mismatch! [Exp] {1} != [Act] {2}", success, expected.Status, p.Status);
                    
                    break;
                }
            }
            
            if(!found)
                success = string.Format("{0}\r\n ***ERROR: [SearchPitch()] Failed to find pitch! [Exp] {1}", success, JsonConvert.SerializeObject(expected));
                
            session.Dispose();
            
            return success;
        }

        public static string GetBriefTitle(string BriefId)
        {
            string title = "";
            
            var session = RavenClient.OpenRavenSession();
            var b = session.Load<Brief>(BriefId);
            title = b.Title;
            session.Dispose();
            
            return title;
        }
        
        public static Brief GetRandomBrief(bool IsPrivate, string TestName = "")
        {
            string status = (IsPrivate)? "Private" : "Public";
            
            var session = RavenClient.OpenRavenSession();
            
            var results = (from b in session.Query<Brief>() where b.Status.Equals(status) select b).ToList<Brief>();
            
            session.Dispose();
            
            int i = Randomizer.GetRandomNumber(0, results.Count());
            
            if(!TestName.Equals(""))
                System.IO.File.WriteAllText(string.Format("C:\\Users\\Public\\{0}.txt", TestName), JsonConvert.SerializeObject(results[i]));
            
            return results[i];
        }
        
        public static Pitcher GetRandomPitcher(string TestName = "")
        {
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitcher>() where p.LastName.StartsWith("Pitcher") select p).ToList<Pitcher>();
            session.Dispose();
            
            var obj = results[Randomizer.GetRandomNumber(0, results.Count())];
            
            if(!TestName.Equals(""))
                System.IO.File.WriteAllText(string.Format("C:\\Users\\Public\\{0}.txt", TestName), JsonConvert.SerializeObject(obj));
            
            return obj;
        }
        
        public static string GetBriefId(string Title)
        {
            string id = string.Empty;
            
            var session = RavenClient.OpenRavenSession();
            //var results = session.Query<Brief>().ToList();
            var results = (from b in session.Query<Brief>() where b.Title.StartsWith(Title) select b).ToList<Brief>();
            
            foreach(Brief b in results)
            {                
                if(Title.Equals(b.Title))
                {
                    id = b.Id;
                    break;
                }
            }            
            session.Dispose();            
            return id;
        }
        
        public static string SearchBrief(Brief expected)
        {
            string success = string.Empty;
            bool found = false;
            
            var session = RavenClient.OpenRavenSession();
            //var results = session.Query<Brief>().ToList();
            var results = (from b in session.Query<Brief>() where b.Title.StartsWith(expected.Title) select b).ToList<Brief>();
            
            foreach(Brief b in results)
            {                
                if(expected.Title.Equals(b.Title))
                {
                    found = true;
                    if(!expected.Description.Equals(b.Description) ||
                        !expected.Location.Equals(b.Location) ||
                        !expected.Price.Equals(b.Price))
                        success = string.Format("{0}\r\n ***ERROR: Incorrect Brief Details! \r\n [Exp] {1} != \r\n [Act] {2}", success, JsonConvert.SerializeObject(expected), JsonConvert.SerializeObject(b));
                    
                    var p = session.Load<Buyer>(b.BuyerId);
                    
                    if(p != null)
                    {
                        Console.WriteLine(string.Format("expected:{0}\r\n actual:{1}", JsonConvert.SerializeObject(expected.Publisher), JsonConvert.SerializeObject(p)));
                        if(!expected.Publisher.FirstName.Equals(p.FirstName) ||
                            !expected.Publisher.LastName.Equals(p.LastName) ||
                            !expected.Publisher.Company.Equals(p.Company) ||
                            !expected.Publisher.Email.Equals(p.Email) ||
                            !expected.Publisher.Phone.Equals(p.Phone))
                            success = string.Format("{0}\r\n ***ERROR: Incorrect Buyer Details! \r\n [Exp] {1} != \r\n [Act] {2}", success, JsonConvert.SerializeObject(expected.Publisher), JsonConvert.SerializeObject(p));
                    }
                    else
                        success = string.Format("{0}\r\n ***ERROR: Failed to find buyer with id {1}", b.BuyerId);
                    break;
                }
            }
            
            if(!found)
                success = string.Format("{0}\r\n ***ERROR: Failed to find brief! [Exp] {1}", success, JsonConvert.SerializeObject(expected));
            
            session.Dispose();            
            
            return success;
        }
    
        public static string VerifyBriefStatus(Brief expected)
        {
            string success = string.Empty;            
            bool found = false;
            
            var session = RavenClient.OpenRavenSession();
            var results = (from b in session.Query<Brief>() where b.Title.StartsWith(expected.Title) select b).ToList<Brief>();
            
            foreach(Brief b in results)
            {
                if(expected.Title.Equals(b.Title))
                {
                    found = true;                   
                    if(!expected.Status.Equals(b.Status))
                        success = string.Format("{0}\r\n ***ERROR: Status mismatch! [Exp] {1} != [Act] {2}", success, expected.Status, b.Status);
                   if(!expected.AdminStatus.Equals(b.AdminStatus))
                        success = string.Format("{0}\r\n ***ERROR: Status mismatch! [Exp] {1} != [Act] {2}", success, expected.AdminStatus, b.AdminStatus);
                  //  if(expected.IsDeleted != p.IsDeleted)
                  //      success = string.Format("{0}\r\n ***ERROR: IsDeleted mismatch! [Exp] {1} != [Act] {2}", success, expected.IsDeleted.ToString(), p.IsDeleted.ToString());
                    
                    break;
                }
            }
            
            if(!found)
                success = string.Format("{0}\r\n ***ERROR: [VerifyBriefStatus()]Failed to find brief! [Exp] {1}", success, JsonConvert.SerializeObject(expected));
                
            session.Dispose();
            
            return success;
        }
        
        public static string SearchPitcher(Pitcher pitcher, bool ComparePhone = false)
        {
            string success = string.Empty;
            bool found = false;
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitcher>() where p.LastName.StartsWith("Pitcher") select p).ToList<Pitcher>();           
            
            success = (string.Format("{0}\r\n***TRACE: [SearchPitcher()] Results Count {1}\r\n {2}", success, results.Count(), JsonConvert.SerializeObject(pitcher)));
            
            foreach(Pitcher p in results)
            {
                success = (string.Format("{0}\r\n***TRACE: {1}", success, p.Email));
                if(p.Email.Equals(pitcher.Email))
                {
                    if(!p.FirstName.Equals(pitcher.FirstName))
                        success =string.Format("{0}\r\n ***ERROR: [SearchPitcher()] FirstName miscompare! [Exp] {1} != [Act] {2}", success, pitcher.FirstName, p.FirstName);
                    if(!p.LastName.Equals(pitcher.LastName))
                        success =string.Format("{0}\r\n ***ERROR: [SearchPitcher()] LastName miscompare! [Exp] {1} != [Act] {2}", success, pitcher.LastName, p.LastName);
                    if(ComparePhone)
                    {
                        if(p.Phone.Equals(null))
                            success =string.Format("{0}\r\n ***ERROR: [SearchPitcher()] Phone is NULL! [Exp] {1} != [Act] NULL", success, pitcher.Phone);
                        else if(!p.Phone.Equals(pitcher.Phone))
                            success =string.Format("{0}\r\n ***ERROR: [SearchPitcher()] Phone miscompare! [Exp] {1} != [Act] {2}", success, pitcher.Phone, p.Phone);
                    }
                    if(p.SendMail != pitcher.SendMail)
                        success =string.Format("{0}\r\n ***ERROR: [SearchPitcher()] SendMail miscompare! [Exp] {1} != [Act] {2}", success, pitcher.SendMail.ToString(), p.SendMail.ToString());
                    found = true;
                    break;
                }
            }
            
            session.Dispose();
            
            if(!found)
                success = string.Format("{0}\r\n ***ERROR: [SearchPitcher()] Failed to find pitcher! {1}", success, JsonConvert.SerializeObject(pitcher));
            
            return success;
        }        
        
        public static Pitcher GetNextFreelancer(string TestName)
        {
            string path = string.Format("C:\\Users\\Public\\{0}.txt", TestName);
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitcher>() where p.FirstName.StartsWith("Freelancer") select p).ToList<Pitcher>();
            int idx = 0;
            foreach(Pitcher p in results)
            {
                int num = System.Convert.ToInt32(p.LastName.Split(' ')[1]);
                idx = (num > idx)? num: idx;
            }
            
            idx += 1;
            
            session.Dispose();
            
            var obj = new Pitcher()
            {
                FirstName = "Freelancer",
                LastName = "Pitcher " + idx.ToString(),
                Email = "qa+" + "freelancer" + idx.ToString() + "@oxygenventures.com.au",
                Phone = "Freelancer Phone " + idx.ToString(),
                SendMail = false
            }; 
            
            System.IO.File.WriteAllText(path, JsonConvert.SerializeObject(obj));            
            return obj;
        }
        
        public static void CreateRepitchTestData()
        {
            try
            {
                var session = RavenClient.OpenRavenSession();
                
                for(int n = 0; n < 2; n++)
                {
                    string pitcher = "";
                    switch(n % 2)
                    {
                        case 0: pitcher = "pitchers/2035"; break; // sendmail true
                        case 1: pitcher = "pitchers/2404"; break; // sendmail false
                    }
                    
                    for(int i = 0; i < 2; i++)
                    {
                        string briefId = "";
                        switch(i % 2)
                        {
                            case 0: briefId = "briefs/1889"; break;
                            case 1: briefId = string.Empty; break;
                        }
                        for(int j = 0; j < 2; j++)
                        {
                            bool repitch = false;
                            switch(j % 2)
                            {
                                case 0: repitch = true; break;
                                case 1: repitch = false; break;
                            }
                            
                            for(int k = 0; k < 4; k++)
                            {
                                string status = "";
                                switch(k % 4)
                                {
                                    case 0: status = "Received"; break;
                                    case 1: status = "SendToCrm"; break;
                                    case 2: status = "RequestedInfo"; break;
                                    case 3: status = "Unsuccessful"; break;                                    
                                }
                                
                                for(int m = 0; m < 10; m++)
                                {
                                    DateTime d = DateTime.UtcNow;
                                    switch(m % 10)
                                    {
                                        case 0: d = DateTime.UtcNow; break;
                                        case 1: d = DateTime.UtcNow.AddDays(-1); break;
                                        case 2: d = DateTime.UtcNow.AddDays(-2); break;
                                        case 3: d = DateTime.UtcNow.AddDays(-3); break;
                                        case 4: d = DateTime.UtcNow.AddDays(-4); break;
                                        case 5: d = DateTime.UtcNow.AddDays(-5); break;
                                        case 6: d = DateTime.UtcNow.AddDays(-6); break;
                                        case 7: d = DateTime.UtcNow.AddMonths(-1); break;
                                        case 8: d = DateTime.UtcNow.AddMonths(-1).AddDays(-3); break;
                                        case 9: d = DateTime.UtcNow.AddMonths(-1).AddDays(-5); break;
                                    }
                                    
                                    Pitch p = new Pitch();
                                    p.Title = string.Format("Kristine Title {0} {1} Repitch {2} Status {3} {4}", pitcher, briefId, repitch, status, d.ToString());
                                    p.Location = Randomizer.GetRandomLocation();
                                    p.Price = (((n+1)*(i+1)*(j+1)*(k+1))+m).ToString();
                                    p.Description = string.Format(" Pitcher {0}\r\n Brief Id {1}\r\n Repitch {2}\r\n Status {3}\r\n Date created {4}", pitcher, briefId, repitch.ToString(), status, d.ToString());
                                    p.Point1 = string.Format("Repitch {0}", repitch.ToString());
                                    p.Point2 = string.Format("Status {0}", status);
                                    p.Point3 = string.Format("{0}", d.ToString());
                                    p.CreatedDate = d;
                                    p.PitcherId = pitcher;
                                    p.IsFavorite = false;                    
                                    p.BriefId = briefId;
                                    p.Deleted = false;
                                    p.Quotes = briefId;
                                    p.AlreadySent = false;
                                    p.Status = status;
                                    p.Repitch = repitch;
                                    session.Store(p);
                                }
                            }
                        }
                    }
                }

                session.SaveChanges();
                session.Dispose();
            }
            catch(Exception e)
            {
                Console.WriteLine("Error creating object \r\n" + e.Message);
            }
        }
        
        public void CreatePitches()
        {
            try
            {
                var session = RavenClient.OpenRavenSession();
                
                for(int i = 400; i < 550; i++)
                {
                    Pitch p = new Pitch();
                    p.Title = string.Format("Nap Solicited Pitch Title{0}", i);
                    p.Location = Randomizer.GetRandomLocation();
                    p.Price = i.ToString();
                    p.Description = string.Format("Nap Solicited Pitch Description {0}", i);
                    p.Point1 = string.Format("Nap Solicited Pitch Point1 {0}", i);
                    p.Point2 = string.Format("Nap Solicited Pitch Point2 {0}", i);
                    p.Point3 = string.Format("Nap Solicited Pitch Pitch3 {0}", i);
                    p.CreatedDate = System.Convert.ToDateTime("2013-10-22T08:48:27.0006392");
                    p.PitcherId = "pitchers/4835";
                    p.IsFavorite = false;                    
                    p.BriefId = "briefs/2274";
                    //p.BriefId = null;
                    p.Deleted = false;
                    p.Quotes = string.Format("Nap Solicited Pitch Quotes {0}", i);
                    p.AlreadySent = false;
                    p.Status = "Received";
                    session.Store(p);
                }
                
                session.SaveChanges();
                session.Dispose();
            }
            catch(Exception e)
            {
                Console.WriteLine("Error creating object \r\n" + e.Message);
            }
        }
        
        public void CreateBriefs()
        {
            try
            {
                var session = RavenClient.OpenRavenSession();
                
                for(int i = 0; i < 150; i++)
                {
                    Brief b = new Brief();
                    b.AdminStatus = "New";
                    b.BuyerId = "buyers/1953";
                    b.Commissioned = false;
                    b.Complete = false;
                    b.CreatedDate = System.Convert.ToDateTime("2013-10-21T08:48:27.0006392");
                    b.Deleted = false;
                    b.Description = string.Format("Brief Description {0}", i);
                    b.Location = Randomizer.GetRandomLocation();
                    b.Price = i.ToString();
                    b.Sent = false;
                    b.Status = "Private";
                    b.Title = string.Format("Brief Title {0}", i);
                    session.Store(b);
                }
                
                session.SaveChanges();
                session.Dispose();
            }
            catch(Exception e)
            {
                Console.WriteLine("Error creating object \r\n" + e.Message);
            }
        }
        
        public static string CheckRepitchTestDataStatus()
        {
            //string success = string.Empty;
            string success = "***ERROR";            
            
            var session = RavenClient.OpenRavenSession();
            
            var results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch False") && p.Point2.Equals("Status Received") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [1] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                DateTime d = System.Convert.ToDateTime(p.Point3);
                
                if((System.DateTime.UtcNow - d).TotalDays >= 5)
                //if(d.AddDays(5) < System.DateTime.UtcNow.Date)
                {
                    success = string.Format("{0}\r\n ***TRACE: [1] Checking {1}", success, p.Id);
                    if(!p.Status.Equals("Unsuccessful"))
                        success = string.Format("{0}\r\n ***ERROR: [1] Status Error {1}. Must be Unsuccessful", success, p.Id);                    
                    if(!p.BriefId.Equals(p.Quotes))
                        success = string.Format("{0}\r\n ***ERROR: [1] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                    if(p.Repitch != false)
                        success = string.Format("{0}\r\n ***ERROR: [1] Repitch Error {1}. Must be false", success, p.Id);
                    if(!p.Point3.Equals(p.CreatedDate.ToString()))
                        success = string.Format("{0}\r\n ***ERROR: [1] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);                    
                }
            }
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch False") && p.Point2.Equals("Status SendToCrm") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [2] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [2] Checking {1}", success, p.Id);
                if(!p.Status.Equals("SendToCrm"))
                    success = string.Format("{0}\r\n ***ERROR: [2] Status Error {1}. Must be SendToCrm", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [2] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != false)
                    success = string.Format("{0}\r\n ***ERROR: [2] Repitch Error {1}. Must be false", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [2] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch False") && p.Point2.Equals("Status RequestedInfo") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [3] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [3] Checking {1}", success, p.Id);
                if(!p.Status.Equals("RequestedInfo"))
                    success = string.Format("{0}\r\n ***ERROR: [3] Status Error {1}. Must be RequestedInfo", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [3] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != false)
                    success = string.Format("{0}\r\n ***ERROR: [3] Repitch Error {1}. Must be false", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [3] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch False") && p.Point2.Equals("Status Unsuccessful") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [4] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [4] Checking {1}", success, p.Id);
                if(!p.Status.Equals("Unsuccessful"))
                    success = string.Format("{0}\r\n ***ERROR: [4] Status Error {1}. Must be Unsuccessful", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [4] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != false)
                    success = string.Format("{0}\r\n ***ERROR: [4] Repitch Error {1}. Must be false", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [4] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch True") && p.Point2.Equals("Status Received") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [5] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                DateTime d = System.Convert.ToDateTime(p.Point3);
                //if(d.AddDays(3) < System.DateTime.UtcNow.Date)
                if((System.DateTime.UtcNow - d).TotalDays >= 3)
                {
                    success = string.Format("{0}\r\n ***TRACE: [5] Checking {1}", success, p.Id);
                    if(!p.Status.Equals("Unsuccessful"))
                        success = string.Format("{0}\r\n ***ERROR: [5] Status Error {1}. Must be Unsuccessful", success, p.Id);
                    if(!string.IsNullOrEmpty(p.BriefId))
                        success = string.Format("{0}\r\n ***ERROR: [5] BriefId Error {1}. Must be null", success, p.Id);
                    if(p.Repitch != true)
                        success = string.Format("{0}\r\n ***ERROR: [5] Repitch Error {1}. Must be true", success, p.Id);
                    if(!p.Point3.Equals(p.CreatedDate.ToString()))
                        success = string.Format("{0}\r\n ***ERROR: [5] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);                    
                }
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch True") && p.Point2.Equals("Status SendToCrm") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [6] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [6] Checking {1}", success, p.Id);
                if(!p.Status.Equals("SendToCrm"))
                    success = string.Format("{0}\r\n ***ERROR: [6] Status Error {1}. Must be SendToCrm", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [6] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != true)
                    success = string.Format("{0}\r\n ***ERROR: [6] Repitch Error {1}. Must be true", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [6] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch True") && p.Point2.Equals("Status RequestedInfo") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [7] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [7] Checking {1}", success, p.Id);
                if(!p.Status.Equals("RequestedInfo"))
                    success = string.Format("{0}\r\n ***ERROR: [7] Status Error {1}. Must be RequestedInfo", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [7] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != true)
                    success = string.Format("{0}\r\n ***ERROR: [7] Repitch Error {1}. Must be true", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [7] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            session = RavenClient.OpenRavenSession();
            results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch True") && p.Point2.Equals("Status Unsuccessful") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [8] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                success = string.Format("{0}\r\n ***TRACE: [8] Checking {1}", success, p.Id);
                if(!p.Status.Equals("Unsuccessful"))
                    success = string.Format("{0}\r\n ***ERROR: [8] Status Error {1}. Must be Unsuccessful", success, p.Id);
                if(!p.BriefId.Equals(p.Quotes))
                    success = string.Format("{0}\r\n ***ERROR: [8] BriefId Error {1}. Must be {2}", success, p.Id, p.Quotes);
                if(p.Repitch != true)
                    success = string.Format("{0}\r\n ***ERROR: [8] Repitch Error {1}. Must be true", success, p.Id);
                if(!p.Point3.Equals(p.CreatedDate.ToString()))
                    success = string.Format("{0}\r\n ***ERROR: [8] Created Date Error {1}. Must be {2}", success, p.Id, p.Point3);
            }
            
            session.Dispose();
            
            return success;
        }
        
        public static string CheckRepitchTestDataStatusAfterRepitch()
        {
            string success = "***ERROR";
            // after re-pitch, check the following
            var session = RavenClient.OpenRavenSession();
            var results = (from p in session.Query<Pitch>() where p.Point1.Equals("Repitch False") && p.Point2.Equals("Status Received") select p).ToList<Pitch>();
            success = string.Format("{0}\r\n ***TRACE: [9] results count: {1}", success, results.Count());
            foreach(Pitch p in results)
            {
                DateTime d = System.Convert.ToDateTime(p.Point3);
                //if(d.AddDays(-5) < System.DateTime.UtcNow.Date)
                if((System.DateTime.UtcNow - d).TotalDays >= 5)
                {
                    success = string.Format("{0}\r\n ***TRACE: [9] Checking {1}", success, p.Id);
                    if(!p.Status.Equals("Received"))
                        success = string.Format("{0}\r\n ***ERROR: [9] Status Error {1}. Must be Received", success, p.Id);
                    if(!string.IsNullOrEmpty(p.BriefId))
                        success = string.Format("{0}\r\n ***ERROR: [6] BriefId Error {1}. Must be null", success, p.Id);
                    if(!p.Repitch)
                        success = string.Format("{0}\r\n ***ERROR: [9] Repitch Error {1}. Must be true", success, p.Id);
                    if(DateTime.Compare(p.CreatedDate, System.Convert.ToDateTime(p.Point3)) <= 0)
                        success = string.Format("{0}\r\n ***ERROR: [9] Created Date Error {1}. Must be reset!", success, p.Id);
                }
            }
            
            session.Dispose();
            return success;
        }
        
        public static List<Brief> GetBriefsBasedOnCountry(string country)
        {
            var session = RavenClient.OpenRavenSession();
            List<Brief> results = new List<Brief>();
            
            if(country.Equals("All"))
                results = (from p in session.Query<Brief>() where (!p.Complete && !p.Deleted && p.Status.Equals("Public")) select p).Take(1000).ToList<Brief>();
            
            else
                results = (from p in session.Query<Brief>() where (p.Country.Equals(country) && !p.Complete && !p.Deleted && p.Status.Equals("Public")) select p).Take(1000).ToList<Brief>();
            
            session.Dispose();
            return results;
        }

        [CodedStep(@"Debug")]
        public void Debug_CodedStep()
        {
            
            //string path = string.Format("C:\\Users\\Public\\{0}.txt", Data["TestName"].ToString());
            //var p = RavenClient.GetNextPitch(Data["TestName"].ToString());
            //var b = RavenClient.GetNextBrief(Data["TestName"].ToString());             
            
            /*
            var id = RavenClient.GetBriefId("Private Brief 35");
            Log.WriteLine("Kristine: " + id);
            
            if(id == string.Empty)
                Log.WriteLine("***ERROR: Failed to find brief! [Exp] Title: Private Brief 35");
            */
            
            //var b = RavenClient.GetRandomBrief(false);
            //Log.WriteLine("Kristine: " + b.Id);
            
            //var p = RavenClient.GetNextPitch("Test1", true, false);
            //Log.WriteLine("Kristine Existing Pitcher: " + p.Reporter.FirstName + " " + p.Reporter.LastName);
            
            //p = RavenClient.GetNextPitch("Test2");
            //Log.WriteLine("Kristine New Pitcher: " + p.Reporter.FirstName + " " + p.Reporter.LastName);
            
            /*
            var reporter = RavenClient.GetRandomPitcher();
            Log.WriteLine(JsonConvert.SerializeObject(reporter));
            string success = RavenClient.SearchPitcher(reporter);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("GetRandomPitcher():: Search pitcher error! {0}", success));
            reporter.Update("Updated");
            Log.WriteLine(JsonConvert.SerializeObject(reporter));
            
            reporter = RavenClient.GetNextFreelancer("Debug");
            success = RavenClient.SearchPitcher(reporter);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("GetNextFreelancer():: Search pitcher error! {0}", success));
            Log.WriteLine(JsonConvert.SerializeObject(reporter));
            */
            
            //RavenClient.CreateRepitchTestData();
            
            //string success = RavenClient.CheckRepitchTestDataStatus();
            //Assert.IsFalse(success.Contains("***ERROR"), success);
            
            //string success = RavenClient.CheckRepitchTestDataStatusAfterRepitch();
            //Assert.IsFalse(success.Contains("***ERROR"), success);            
            
            var results = RavenClient.GetBriefsBasedOnCountry("All");
            Log.WriteLine(string.Format("All count: {0}", results.Count.ToString()));
            results = RavenClient.GetBriefsBasedOnCountry("Global");
            Log.WriteLine(string.Format("Global count: {0}", results.Count.ToString()));            
        }        
    }
}
