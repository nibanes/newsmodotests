using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;
using System.Security.Cryptography;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class Randomizer : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...
    
        public static int GetRandomNumber(int min, int max)
        {
            Random Rg = new Random(GenerateSeed());
            int n = Rg.Next(min, max);            
            return n;
        }
        
        public static bool GetRandomBool()
        {            
            System.Threading.Thread.Sleep(200);
            return ((Randomizer.GetRandomNumber(0, 100) < 50)? true: false);
        }
        
        public static int GenerateSeed()
        {
            // Use a 4-byte array to fill it with random bytes and convert it then
            // to an integer value.
            byte[] randomBytes = new byte[4];

            // Generate 4 random bytes.
            RNGCryptoServiceProvider rng = new RNGCryptoServiceProvider();
            rng.GetBytes(randomBytes);

            // Convert 4 bytes into a 32-bit integer value.
            int seed = (randomBytes[0] & 0x7f) << 24 |
                        randomBytes[1] << 16 |
                        randomBytes[2] << 8 |
                        randomBytes[3];

            return seed;
        }
        
        public static string GetRandomLocation()
        {
            string location = "Melbourne, Australia";
            switch(Randomizer.GetRandomNumber(0, 9))
            {
                case 1: location = "Victoria, Australia"; break;
                case 2: location = "Sydney, Australia"; break;
                case 3: location = "Manila, Philippines"; break;
                case 4: location = "Singapore, Singapore"; break;
                case 5: location = "Hong Kong"; break;
                case 6: location = "Maryland, USA"; break;
                case 7: location = "Bangkok, Thailand"; break;
                case 8: location = "London, UK"; break;
                case 9: location = "Nowhere"; break;
            }
            
            return location;
        }
        
        public static string GetRandomCountry()
        {
            string country = "Australia";
            switch(Randomizer.GetRandomNumber(0, 9))
            {
                case 1: country = "Austria"; break;
                case 2: country = "Algeria"; break;
                case 3: country = "Philippines"; break;
                case 4: country = "Singapore"; break;
                case 5: country = "China"; break;
                case 6: country = "United States"; break;
                case 7: country = "Thailand"; break;
                case 8: country = "United Kingdom"; break;
                case 9: country = "Turkey"; break;
            }
            
            return country;
        }
        
        public static string GetRandomPitchStatus()
        {
            string status = "Received";
            switch(Randomizer.GetRandomNumber(0, 3))
            {
                case 1: status = "Requested Info"; break;
                case 2: status = "Send To Crm"; break;
                case 3: status = "Commissioned"; break;                    
            }
            
            return status;
        }
        
        public static string GetRandomParameters()
        {
            string something = "";
            
            switch(Randomizer.GetRandomNumber(0, 9))
            {
                case 0: something = "something small and hairy"; break;
                case 1: something = "something shocking and cute"; break;
                case 2: something = "something twisted and mysterious"; break;
                case 3: something = "something round and blue"; break;
                case 4: something = "something for Private Use"; break;
                case 5: something = "something that starts with a “C” and ends with “E”"; break;
                //case 6: something = "something chewy and flavorful"; break;
                case 6: something = "something fluffy and sweet"; break;
                case 7: something = "something old and heavy"; break;
                case 8: something = "something funny and stress-free"; break;
                case 9: something = "something slimy and sticky"; break;
            }
            
            return something;
        }
        
        [CodedStep(@"Debug")]
        public void Debug_CodedStep()
        {
            Log.WriteLine(string.Format("Theme for 11 Nov 2013: {0}", Randomizer.GetRandomParameters()));
        }
    }
}
