using Telerik.TestingFramework.Controls.KendoUI;
using Telerik.WebAii.Controls.Html;
using Telerik.WebAii.Controls.Xaml;
using System;
using System.Collections.Generic;
using System.Text;
using System.Linq;

using Newtonsoft.Json;

using ArtOfTest.Common.UnitTesting;
using ArtOfTest.WebAii.Core;
using ArtOfTest.WebAii.Controls.HtmlControls;
using ArtOfTest.WebAii.Controls.HtmlControls.HtmlAsserts;
using ArtOfTest.WebAii.Design;
using ArtOfTest.WebAii.Design.Execution;
using ArtOfTest.WebAii.ObjectModel;
using ArtOfTest.WebAii.Silverlight;
using ArtOfTest.WebAii.Silverlight.UI;

namespace NMLite_Telerik
{

    //
    // You can add custom execution steps by simply
    // adding a void function and decorating it with the [CodedStep] 
    // attribute to the test method. 
    // Those steps will automatically show up in the test steps on save.
    //
    // The BaseWebAiiTest exposes all key objects that you can use
    // to access the current testcase context. [i.e. ActiveBrowser, Find ..etc]
    //
    // Data driven tests can use the Data[columnIndex] or Data["columnName"] 
    // to access data for a specific data iteration.
    //
    // Example:
    //
    // [CodedStep("MyCustom Step Description")]
    // public void MyCustomStep()
    // {
    //        // Custom code goes here
    //      ActiveBrowser.NavigateTo("http://www.google.com");
    //
    //        // Or
    //        ActiveBrowser.NavigateTo(Data["url"]);
    // }
    //
        

    public class SendBriefToEmail : BaseWebAiiTest
    {
        #region [ Dynamic Pages Reference ]

        private Pages _pages;

        /// <summary>
        /// Gets the Pages object that has references
        /// to all the elements, frames or regions
        /// in this project.
        /// </summary>
        public Pages Pages
        {
            get
            {
                if (_pages == null)
                {
                    _pages = new Pages(Manager.Current);
                }
                return _pages;
            }
        }

        #endregion
        
        // Add your test methods here...    
        Brief Brief;
        
        [CodedStep(@"Get Brief Details")]
        public void FindBrief_CodedStep()
        {
            var content = FileReaderUtility.ReadFile(Data["TestName"].ToString());
            this.Brief = JsonConvert.DeserializeObject<Brief>(content);
        }     
    
        [CodedStep(@"Verify that brief is added in email list")]
        public void FindBriefInEmailTemplate_CodedStep()
        {
            var e = Pages.NewsmodoAdmin.SendBriefsToContributorsBriefDetailsDiv.Find.ByExpression(string.Format("textcontent={0}", this.Brief.Description));
            Assert.IsTrue(e != null, string.Format("***ERROR: Failed to find Brief in Brief List! {0}", this.Brief.Description));
        }
        
        [CodedStep(@"Verify AdminStatus in database")]
        public void CheckAdminStatusInDb_CodedStep()
        {
            this.Brief.AdminStatus = "Sent";  
            this.Brief.Status = "Private";            
            string success = RavenClient.VerifyBriefStatus(this.Brief);
            Assert.IsTrue(!success.Contains("***ERROR"), string.Format("***ERROR: Status mismatch for sent brief! \r\n {0}", success));
        }
    }
}